from django.db import models
from stdimage import StdImageField

# Create your models here.
class About(models.Model):
    location = models.CharField(max_length=30)
    address = models.TextField(null=True)
    text = models.TextField(null=True)
    image = StdImageField(upload_to='aboutimage', blank=True, null=True,variations={'large':(1200, 800,True),'thumbnail': (200, 200,True),})

class Testimonial(models.Model):
	name = models.TextField(null=True)
	text = models.TextField(null=True)
	image = StdImageField(upload_to='aboutimage', blank=True, null=True,variations={'large':(1200, 800,True),'thumbnail': (200, 200,True),})
	name1 = models.TextField(null=True)
	text1 = models.TextField(null=True)
	image1 = StdImageField(upload_to='aboutimage', blank=True, null=True,variations={'large':(1200, 800,True),'thumbnail': (200, 200,True),})
    