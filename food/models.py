
from django.db import models
from stdimage import StdImageField
from django_jalali.db import models as jmodels
from django.utils import timezone
from django.utils.timezone import utc


FOOD_CHOICES = (
    ('coffee',u'قهوه'),
    ('tea', u'چای'),
    ('drink',u'نوشیدنی'),
    ('meal',u'عصرانه'),
    ('breakfast',u'صبحانه'),
)

CHAIR_NUM = (
	('1',u'میز یک'),
	('2',u'میز دو'),
	('3',u'میز سه'),
	('4',u'میز چهار'),
	('5',u'میز بنج'),
	('6',u'میز شش'),
	('7',u'میز بار'),
)

# Create your models here.
class Food(models.Model):
	"""docstring for ClassName"""
	title = models.CharField(max_length=200)
	text = models.TextField(null=True)
	image = StdImageField(upload_to='foodsimg',blank=True,null=True,variations={'thumbnail': (300, 200,True),})
	food = models.CharField(max_length=6,choices=FOOD_CHOICES,default='coffee')
	price = models.DecimalField(null=False,max_digits=10, decimal_places=0)

	def __str__(self):
		return "%s, %s"%(self.title, self.food)

class Order(models.Model):
	chairnum = models.CharField(max_length=12,choices=CHAIR_NUM,default='1')
	foodcount = models.DecimalField(null=False,max_digits=2,decimal_places=0)
	orderfood = models.ForeignKey(Food, on_delete=models.CASCADE,null=True)
	date = jmodels.jDateField(default=timezone.now)
	time = models.TimeField(default=timezone.now)
	archive = models.BooleanField(default=False)
	
	def __str__(self):
		return self.chairnum
