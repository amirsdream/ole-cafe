from django.contrib import admin
from .models import Food

# Register your models here.

class FoodModelAdmin(admin.ModelAdmin):
    list_filter = ['title']
    search_fields = ['title']

    class Meta:
        model = Food


admin.site.register(Food, FoodModelAdmin )