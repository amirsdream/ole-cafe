from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from .models import Slider, Service
from food.models import Food, Order
from gallery.models import Gallery
from cpanel.models import Profile
from blog.models import Blog
from django.views.generic import UpdateView
from django.core.urlresolvers import reverse_lazy
from .forms import OrderForm
from about.models import About, Testimonial
# Create your views here.

def post_list(request):
	images = Slider.objects.all()
	foods = Food.objects.all()
	orders = Order.objects.all()
	services = Service.objects.all()
	profiles = Profile.objects.all()
	galleries = Gallery.objects.all()
	blogs = Blog.objects.all()
	about = About.objects.all()
	testimonials = Testimonial.objects.all()
	return render(request, 'index.html',{'testimonials':testimonials,'about':about,'blogs':blogs,'galleries':galleries,'images':images,'foods':foods,'services':services,'profiles':profiles,'orders':orders})
	
def order(request,pk):
	food = get_object_or_404(Food, pk=pk)
	if request.method == "POST":
		form = OrderForm(request.POST)
		if form.is_valid():
			order = form.save(commit=False)
			order.orderfood = food
			order.save()
			return redirect('post_list')
	else:
		form = OrderForm()
	return render(request, 'order.html', {'form': form,'food':food})