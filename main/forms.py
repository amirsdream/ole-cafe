from django import forms
from food.models import Order
# If you don't do this you cannot use Bootstrap CSS


class OrderForm(forms.ModelForm):

	class Meta:
		model = Order
		fields = ( 'chairnum', 'foodcount', )
		widgets = {
			'chairnum': forms.Select(attrs={'class': 'btn btn-default dropdown-toggle'})
        }
		labels = {
			'chairnum': u'شماره میز',
			'foodcount': u'تعداد غذا',
		}
