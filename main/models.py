from django.db import models
from stdimage import StdImageField

SERVICE_CHOICES = (
    ('wedding', u'عروسی'),
    ('party', u'جشن'),
    ('birthday', u'تولد'),
    ('delivery', u'ارسال'),
)
# Create your models here.


class Slider(models.Model):
    """docstring for Slider"""
    title = models.CharField(max_length=30)
    text = models.TextField(null=True)
    activeslider = models.BooleanField(default=False)
    image = StdImageField(upload_to='productionimg', blank=True, null=True,variations={'large':(1920, 1080,True),'thumbnail': (200, 200,True),})

    def save(self, *args, **kwargs):
        if self.activeslider:
            try:
                temp = Slider.objects.get(activeslider=True)
                if self != temp:
                    temp.activeslider = False
                    temp.save()
            except Slider.DoesNotExist:
                pass

        super(Slider, self).save(*args, **kwargs)


class Service(models.Model):
    service = models.CharField(
        max_length=6, choices=SERVICE_CHOICES, default='wedding')
    wedding = models.TextField(null=True)
    party = models.TextField(null=True)
    birthday = models.TextField(null=True)
    delivery = models.TextField(null=True)
