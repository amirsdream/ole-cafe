from django.contrib import admin
from .models import Slider 

# Register your models here.


class SliderModelAdmin(admin.ModelAdmin):
    list_filter = ['title']
    search_fields = ['title']

    class Meta:
        model = Slider 


admin.site.register(Slider, SliderModelAdmin )