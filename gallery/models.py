from django.db import models
from stdimage import StdImageField

# Create your models here.
class Gallery(models.Model):
	image3 = StdImageField(upload_to='galleryimage', blank=True, null=True,variations={'thumbnail': (300, 200,True),})
