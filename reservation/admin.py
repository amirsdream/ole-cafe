from .models import Reserve
from django.contrib import admin

from django_jalali.admin.filters import JDateFieldListFilter

#you need import this for adding jalali calander widget
import django_jalali.admin as jadmin


class ReserveAdmin(admin.ModelAdmin):
	list_filter = ['name','date']
	search_fields = ['name']

	class Meta:
		model = Reserve

admin.site.register(Reserve, ReserveAdmin)