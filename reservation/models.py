from django.db import models
from django_jalali.db import models as jmodels

# Create your models here.
class Reserve(models.Model):
	"""docstring for ClassName"""
	name = models.CharField(max_length=200)
	count = models.DecimalField(null=False,max_digits=2, decimal_places=0)
	email = models.EmailField(max_length=250)
	date =  jmodels.jDateField()
	phone = models.DecimalField(null=False,max_digits=10, decimal_places=0)
	time = models.TimeField(auto_now=False, auto_now_add=False)
	text = models.TextField(null=True)

	def __str__(self):
		return "%s, %s"%(self.name, self.date)
	

