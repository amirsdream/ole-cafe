from django import forms
from .models import Reserve
class ReserveForm(forms.ModelForm):

    class Meta:
        model = Slider
		fields = ['name','count','email','date','phone','time','text']
        widgets = {
            'name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg'}),
        }
        max_length = {
            'name': 30
        }
