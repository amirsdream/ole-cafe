from django.db import models
from stdimage import StdImageField
# Create your models here.


class Profile(models.Model):
    name = models.CharField(max_length=30)
    desc = models.TextField(null=True)
    image2 = StdImageField(upload_to='profileimage', blank=True, null=True,variations={'thumbnail': (400, 400,True),'thumbnail2': (200, 200,True),})

    def __str__(self):
        return self.name
