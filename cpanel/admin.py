from django.contrib import admin
from .models import Profile

# Register your models here.


class ProfileModelAdmin(admin.ModelAdmin):
    list_filter = ['name']
    search_fields = ['name']

    class Meta:
        model = Profile


admin.site.register(Profile, ProfileModelAdmin )