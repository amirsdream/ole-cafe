from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, SliderForm, FoodForm, ServiceForm, ProfileForm, GalleryForm, BlogForm, AboutForm, TestimonialForm
from main.models import Slider, Service
from food.models import Food, Order
from gallery.models import Gallery
from blog.models import Blog
from .models import Profile
from about.models import About, Testimonial
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import DeleteView, UpdateView, CreateView, ListView
from django.core.urlresolvers import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Sum, F

# Create your views here.


@login_required()
def cpanel(request):
    orderc= Order.objects.filter(archive=False).count()
    foodc= Food.objects.count()
    cprice= Order.objects.filter(archive=True).annotate(count=F('foodcount'),price=F('orderfood__price'))
    cprice= sum([i.count*i.price for i in cprice])/1000
    return render(request, 'admin.html',{'cprice':cprice,'orderc':orderc,'foodc':foodc})

def rating(request):
    orderc= Order.objects.count()
    return render(request, 'orderrefresh.html', {'orderc':orderc})


class SliderList(ListView):
    model = Slider
    template_name = 'slider/slider_edit.html'


class SliderCreate(CreateView):
    model = Slider
    template_name = 'slider/slider.html'
    success_url = reverse_lazy('slider_edit')
    form_class = SliderForm


class SliderUpdate(UpdateView):
    model = Slider
    template_name = 'slider/slider_update_form.html'
    success_url = reverse_lazy('slider_edit')
    form_class = SliderForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            return redirect('delete_slider', pk=self.object.pk)
        else:
            return super(SliderUpdate, self).post(request, *args, **kwargs)


class SliderDelete(DeleteView):
    model = Slider
    template_name = 'slider/delete_confirm.html'
    success_url = reverse_lazy('slider_edit')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(SliderDelete, self).post(request, *args, **kwargs)


class FoodList(ListView):
    model = Food
    template_name = 'food/food_edit.html'


class FoodCreate(CreateView):
    model = Food
    template_name = 'food/food.html'
    success_url = reverse_lazy('food_edit')
    form_class = FoodForm

class FoodUpdate(UpdateView):
    model = Food
    template_name = 'food/food_update_form.html'
    success_url = reverse_lazy('food_edit')
    form_class = FoodForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            return redirect('delete_food', pk=self.object.pk)
        else:
            return super(FoodUpdate, self).post(request, *args, **kwargs)


class FoodDelete(DeleteView):
    model = Food
    template_name = 'food/delete_confirm.html'
    success_url = reverse_lazy('food_edit')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(FoodDelete, self).post(request, *args, **kwargs)


class UpdateModelMixin(object):

    def get_object(self):
        return Service.objects.get_or_create(pk=self.kwargs['pk'])[0]

class ServiceUpdate(UpdateModelMixin,UpdateView):
    model = Service
    template_name = 'service/service_update_form.html'
    form_class = ServiceForm

    def get_success_url(self):
        return reverse('update_service', args=(self.object.pk,))

class UpdateModelMixin(object):

    def get_object(self):
        return Profile.objects.get_or_create(pk=self.kwargs['pk'])[0]

class ProfileUpdate(UpdateModelMixin,UpdateView):
    model = Profile
    template_name = 'user_profile.html'
    form_class = ProfileForm

    def get_success_url(self):
        return reverse('update_profile', args=(self.object.pk,))

class OrderList(ListView):
    model = Order
    template_name = 'order/table.html'
    context_object_name = "order"  
    paginate_by = 10

def deleteOrder(request,pk):
    orders = get_object_or_404(Order, pk=pk)
    orders.delete()
    return redirect('order')

class ArchiveList(ListView):
    model = Order
    template_name = 'order/archtable.html'
    context_object_name = "order"  
    paginate_by = 10


def createArchive(request,pk):
    orders = get_object_or_404(Order, pk=pk)
    orders.archive = True
    orders.save()
    print(orders.archive)
    return redirect('order')


def deleteArchive(request,pk):
    orders = get_object_or_404(Order, pk=pk)
    orders.delete()
    return redirect('archive')


class ArchiveList(ListView):
    model = Order
    template_name = 'order/archtable.html'
    context_object_name = "order"  
    paginate_by = 10

 
class GalleryList(ListView):
    model = Gallery
    template_name = 'gallery/gallery_edit.html'


class GalleryCreate(CreateView):
    model = Gallery
    template_name = 'gallery/gallery.html'
    success_url = reverse_lazy('gallery_edit')
    form_class = GalleryForm


class GalleryUpdate(UpdateView):
    model = Gallery
    template_name = 'gallery/gallery_update_form.html'
    success_url = reverse_lazy('gallery_edit')
    form_class = GalleryForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            return redirect('delete_gallery', pk=self.object.pk)
        else:
            return super(GalleryUpdate, self).post(request, *args, **kwargs)


class GalleryDelete(DeleteView):
    model = Gallery
    template_name = 'gallery/delete_confirm.html'
    success_url = reverse_lazy('gallery_edit')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(GalleryDelete, self).post(request, *args, **kwargs)

class UpdateModelMixin(object):

    def get_object(self):
        return Blog.objects.get_or_create(pk=self.kwargs['pk'])[0]

class BlogUpdate(UpdateModelMixin,UpdateView):
    model = Blog
    template_name = 'blog/blog_update_form.html'
    form_class = BlogForm

    def get_success_url(self):
        return reverse('update_blog', args=(self.object.pk,))

class UpdateModelMixin(object):

    def get_object(self):
        return About.objects.get_or_create(pk=self.kwargs['pk'])[0]

class AboutUpdate(UpdateModelMixin,UpdateView):
    model = About
    template_name = 'about/about.html'
    form_class = AboutForm

    def get_success_url(self):
        return reverse('update_about', args=(self.object.pk,))

class UpdateModelMixin(object):

    def get_object(self):
        return Testimonial.objects.get_or_create(pk=self.kwargs['pk'])[0]

class TestimonialUpdate(UpdateModelMixin,UpdateView):
    model = Testimonial
    template_name = 'about/testimonial.html'
    form_class = TestimonialForm

    def get_success_url(self):
        return reverse('update_testimonial', args=(self.object.pk,))

