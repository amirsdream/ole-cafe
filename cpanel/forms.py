# log/forms.py
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from main.models import Slider, Service
from food.models import Food
from gallery.models import Gallery
from .models import Profile
from blog.models import Blog
from about.models import About, Testimonial
# If you don't do this you cannot use Bootstrap CSS


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput(
        attrs={'type': 'text', 'placeholder': 'نام کاربری ', 'class': 'form-control input-lg', 'name': 'username', 'id': 'username'}))
    password = forms.CharField(label="Password", max_length=30, widget=forms.TextInput(
        attrs={'type': 'password', 'placeholder': 'رمز شما', 'class': 'form-control input-lg', 'name': 'password', 'id': 'password'}))


class SliderForm(forms.ModelForm):

    class Meta:
        model = Slider
        fields = ('title', 'text', 'image', 'activeslider')
        widgets = {
            'title': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'image': forms.FileInput(attrs={'type': 'file', 'class': 'btn btn-success fileinput-button', 'name': 'picture'}),
        }
        labels = {
            'title': u'عنوان',
            'text': u'توضیحات',
            'image': u'عکس',
            'activeslider': u'پیش فرض',
        }
        max_length = {
            'title': 30
        }


class FoodForm(forms.ModelForm):

    class Meta:
        model = Food
        fields = ('title', 'text', 'price', 'food', 'image')
        widgets = {
            'title': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'price': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg', 'name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'image': forms.FileInput(attrs={'type': 'file', 'class': 'btn btn-success fileinput-button', 'name': 'picture'}),
            'food': forms.Select(attrs={'class': 'btn btn-default dropdown-toggle'})
        }
        labels = {
            'title': u'عنوان',
            'text': u'توضیحات',
            'price': u'قیمت',
            'image': u'عکس',
            'food': u'نوع غذا',
        }
        max_length = {
            'title': 30
        }


class ServiceForm(forms.ModelForm):

    class Meta:
        model = Service
        fields = ('wedding', 'party', 'delivery', 'birthday')
        widgets = {
            'wedding': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg ','name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'party': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg ','name':'editor2','id':'editor2','rows':'10','cols':'80'}),
            'delivery': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg ','name':'editor3','id':'editor3','rows':'10','cols':'80'}),
            'birthday': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg ','name':'editor4','id':'editor4','rows':'10','cols':'80'}),
        }
        labels = {
            'wedding': u'عروسی',
            'party': u'‍‍‍پارتی',
            'delivery': u'ارسال',
            'birthday': u'تولد',
        }


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ('name', 'desc', 'image2')
        widgets = {
            'name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'desc': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'image2': forms.FileInput(attrs={'type': 'file', 'class': 'btn btn-success fileinput-button', 'name': 'picture'}),
        }
        labels = {
            'name': u'نام',
            'desc': u'توضیحات',
            'image2': u'عکس',
        }


class GalleryForm(forms.ModelForm):

    class Meta:
        model = Gallery
        fields = ('image3',)
        widgets = {
            'image3': forms.FileInput(attrs={'type': 'file', 'class': 'btn btn-success fileinput-button', 'name': 'picture'}),
        }
        labels = {
            'image3': u'عکس',
        }


class BlogForm(forms.ModelForm):

    class Meta:
        model = Blog
        fields = ('title', 'text', 'image', 'date')
        widgets = {
            'title': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'image': forms.FileInput(attrs={'type': 'file', 'class': 'btn btn-success fileinput-button', 'name': 'picture'}),
        }
        labels = {
            'title': u'عنوان',
            'text': u'توضیحات',
            'image': u'عکس',
            'date': u'تاریخ',
        }
        max_length = {
            'title': 30
        }

class AboutForm(forms.ModelForm):

    class Meta:
        model = About
        fields = ('location', 'text','address', 'image')
        widgets = {
            'location': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'address': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor2','id':'editor2','rows':'10','cols':'80'}),
            'image': forms.FileInput(attrs={'type': 'file', 'class': 'btn btn-success fileinput-button', 'name': 'picture'}),
        }
        labels = {
            'location': u'مکان در گوگل',
            'text' : u'توضیح درباره ما' ,
            'address': u'آدرس',
            'image': u'عکس',     
        }
        max_length = {
            'location': 30
        }

class TestimonialForm(forms.ModelForm):

    class Meta:
        model = Testimonial
        fields = ('name', 'text','name1', 'text1')
        widgets = {
            'name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor1','id':'editor1','rows':'10','cols':'80'}),
            'name1': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated', 'name': 'title', 'id': 'title'}),
            'text1': forms.Textarea(attrs={'type': 'text', 'class': 'form-control input-lg','name':'editor2','id':'editor2','rows':'10','cols':'80'}),
        
        }
        labels = {
            'name': u'مشتری اول',
            'text' : u'نظر' ,
            'name1': u'مشتری دوم',
            'text1': u'نظر',     
        }
        max_length = {
            'name': 30,
            'name1': 30,
        }
