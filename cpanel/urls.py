from django.conf.urls import url, include
from . import views as views
from django.contrib.auth import views as auth_views
from .forms import LoginForm

#cpanel urls
urlpatterns = [
    url(r'^cpanel/$', views.cpanel, name='cpanel'),
    url(r'^cpanel/login/$', auth_views.login,{'template_name': 'login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^cpanel/logout/$', auth_views.logout,{'next_page': 'login'}, name='logout'),
]



#sliders urls
urlpatterns += [
    url(r'^cpanel/slider/$', views.SliderCreate.as_view(), name='slider'),
    url(r'^cpanel/editslider/$', views.SliderList.as_view(), name='slider_edit'),
    url(r'^cpanel/(?P<pk>\d+)/updateslider/$',views.SliderUpdate.as_view(), name='update_slider'),
    url(r'^cpanel/(?P<pk>\d+)/deleteslider/$',views.SliderDelete.as_view(), name='delete_slider'),
]

#food urls
urlpatterns += [
    url(r'^cpanel/food/$', views.FoodCreate.as_view(), name='food'),
    url(r'^cpanel/editfood/$', views.FoodList.as_view(), name='food_edit'),
    url(r'^cpanel/(?P<pk>\d+)/updatefood/$',views.FoodUpdate.as_view(), name='update_food'),
    url(r'^cpanel/(?P<pk>\d+)/deletefood/$',views.FoodDelete.as_view(), name='delete_food'),
]

#gallery urls
urlpatterns += [
    url(r'^cpanel/gallery/$', views.GalleryCreate.as_view(), name='gallery'),
    url(r'^cpanel/editgallery/$', views.GalleryList.as_view(), name='gallery_edit'),
    url(r'^cpanel/(?P<pk>\d+)/updategallery/$',views.GalleryUpdate.as_view(), name='update_gallery'),
    url(r'^cpanel/(?P<pk>\d+)/deletegallery/$',views.GalleryDelete.as_view(), name='delete_gallery'),
]

#service url
urlpatterns += [
    #url(r'^cpanel/service/$', views.ServiceCreate.as_view(), name='service'),
    url(r'^cpanel/(?P<pk>\d+)/updateservice/$',views.ServiceUpdate.as_view(), name='update_service'),
]

#service url
urlpatterns += [
    #url(r'^cpanel/service/$', views.ServiceCreate.as_view(), name='service'),
    url(r'^cpanel/(?P<pk>\d+)/updateblog/$',views.BlogUpdate.as_view(), name='update_blog'),
]

#profile url
urlpatterns += [
    #url(r'^cpanel/profile/$', views.ProfileCreate.as_view(), name='profile'),
    url(r'^cpanel/(?P<pk>\d+)/profile/$',views.ProfileUpdate.as_view(), name='update_profile'),
]

#about url
urlpatterns += [
    url(r'^cpanel/(?P<pk>\d+)/about/$',views.AboutUpdate.as_view(), name='update_about'),
]


#testimonial url
urlpatterns += [
    url(r'^cpanel/(?P<pk>\d+)/testimonial/$',views.TestimonialUpdate.as_view(), name='update_testimonial'),
]

#order url
urlpatterns += [
    url(r'^cpanel/order/$', views.OrderList.as_view(), name='order'),
    url(r'^cpanel/(?P<pk>\d+)/order$',views.deleteOrder, name='deleteorder'),
]

#archive url
urlpatterns += [
    url(r'^cpanel/archive/$', views.ArchiveList.as_view(), name='archive'),
    url(r'^cpanel/(?P<pk>\d+)/archive$',views.createArchive, name='createarch'),
    url(r'^cpanel/(?P<pk>\d+)/archive1$',views.deleteArchive, name='deletearchive'),
]


urlpatterns += [
    url(r'^rating/$',views.rating, name='rating'),
]